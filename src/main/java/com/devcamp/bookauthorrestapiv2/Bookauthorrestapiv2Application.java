package com.devcamp.bookauthorrestapiv2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Bookauthorrestapiv2Application {

	public static void main(String[] args) {
		SpringApplication.run(Bookauthorrestapiv2Application.class, args);
	}

}
