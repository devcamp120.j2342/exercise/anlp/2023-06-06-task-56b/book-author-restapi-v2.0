package com.devcamp.bookauthorrestapiv2.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.bookauthorrestapiv2.models.Author;
import com.devcamp.bookauthorrestapiv2.models.Book;

@RestController
@RequestMapping("/")
@CrossOrigin
public class BookAuthorRestAPIController {
    @GetMapping("/books")
    public ArrayList<Book> getBooks(){
        Author author1 = new Author("An", "a@gmail.com", 'M');
        Author author2 = new Author("Bi", "b@gmail.com", 'M');
        Author author3 = new Author("Du", "d@gmail.com", 'M');
        Author author4 = new Author("Minh", "m@gmail.com", 'F');
        Author author5 = new Author("Hieu", "hieu@gmail.com", 'M');
        Author author6 = new Author("Mai", "mai@gmail.com", 'F');

        System.out.println(author1);
        System.out.println(author2);
        System.out.println(author3);
        System.out.println(author4);
        System.out.println(author5);
        System.out.println(author6);


        ArrayList<Author> arrListAuthor1 = new ArrayList<Author>();
        ArrayList<Author> arrListAuthor2 = new ArrayList<Author>();
        ArrayList<Author> arrListAuthor3 = new ArrayList<Author>();

        arrListAuthor1.add(author4);
        arrListAuthor1.add(author1);

        arrListAuthor2.add(author2);
        arrListAuthor2.add(author3);

        arrListAuthor3.add(author5);
        arrListAuthor3.add(author6);
        
        Author[] authorlist1 = arrListAuthor1.toArray(new Author[0]);
        Author[] authorlist2 = arrListAuthor1.toArray(new Author[1]);
        Author[] authorlist3 = arrListAuthor1.toArray(new Author[2]);


        Book book1 = new Book("ABC", authorlist1, 104500, 1);
        Book book2 = new Book("Toán cao cấp", authorlist2, 75000, 3);
        Book book3 = new Book("Vật lý đại cương", authorlist3, 125000, 2);

        System.out.println(book1);
        System.out.println(book2);
        System.out.println(book3);

        ArrayList<Book> arrListbooks = new ArrayList<Book>();
        arrListbooks.add(book1);
        arrListbooks.add(book2);
        arrListbooks.add(book3);

        return arrListbooks;

    }
}
